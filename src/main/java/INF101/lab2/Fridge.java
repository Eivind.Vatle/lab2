package INF101.lab2;

import javafx.css.Size;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    int max_size=20;
    int numInFridge=0;
    public List<FridgeItem> fridgeList = new ArrayList<>();

    @Override
    public int nItemsInFridge() {
        return fridgeList.size();
    }

    @Override
    public int totalSize() {
        return max_size;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if(fridgeList.size()==20) {
            return false;
        }
        else{
            fridgeList.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (fridgeList.contains(item)) {
            fridgeList.remove(item);
        }
        else {
            throw new NoSuchElementException("Could not find " +item+ " in fridge");
        }



    }

    @Override
    public void emptyFridge() {
        fridgeList.removeAll(fridgeList);

    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        List<FridgeItem>itemsRemoved = new ArrayList<>();
        for(FridgeItem maybeExpired : fridgeList) {
            if(maybeExpired.hasExpired()) {
                itemsRemoved.add(maybeExpired);
            }

        }
        for(FridgeItem removingItem:itemsRemoved) {
            fridgeList.remove(removingItem);

        }

        return itemsRemoved;
    }
}
